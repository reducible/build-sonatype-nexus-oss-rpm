# Build Sonatype Nexus OSS RPMs via docker

Sonatype Nexus OSS is an open source artifact repository with support for many popular formats (e.g. docker, ruby gems, npm, yum, maven etc). 

This repository contains wrapper scripts that allow the building of a Nexus RPM in order to simplify the installation via configuration management tools.

In simple terms, it does the following:

1. Clones [https://github.com/juliogonzalez/nexus-oss-rpms](https://github.com/juliogonzalez/nexus-oss-rpms)
1. Runs `./nexus-oss-rpm -v 3` to generate a Nexus Sonatype v3 RPM for CentOS 7

## Requirements

A working docker instance (e.g. docker on linux/mac).

## Usage

```
./build_latest_v3_rpm_via_docker.sh
```

This should result in an RPM being dropped into your current working directory.

Example output is shown below (this was run on macOS):

```
$ ./build_latest_v3_rpm_via_docker.sh
Cloning / updating nexus-oss-rpms.git
Cloning into 'nexus-oss-rpms'...
remote: Counting objects: 328, done.
remote: Compressing objects: 100% (9/9), done.
remote: Total 328 (delta 3), reused 7 (delta 2), pack-reused 317
Receiving objects: 100% (328/328), 60.91 KiB | 331.00 KiB/s, done.
Resolving deltas: 100% (164/164), done.
Already up to date.
Installing package dependencies... this may take a few minutes
warning: /var/cache/yum/x86_64/7/base/packages/deltarpm-3.6-3.el7.x86_64.rpm: Header V3 RSA/SHA256 Signature, key ID f4a80eb5: NOKEY
Public key for deltarpm-3.6-3.el7.x86_64.rpm is not installed
Importing GPG key 0xF4A80EB5:
 Userid     : "CentOS-7 Key (CentOS 7 Official Signing Key) <security@centos.org>"
 Fingerprint: 6341 ab27 53d7 8a78 a7c2 7bb1 24c6 a8a7 f4a8 0eb5
 Package    : centos-release-7-4.1708.el7.centos.x86_64 (@CentOS)
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
No Presto metadata available for base
Downloading http://download.sonatype.com/nexus/3/nexus-3.12.1-01-unix.tar.gz to ./SOURCES/nexus-3.12.1-01-unix.tar.gz...
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   154  100   154    0     0    602      0 --:--:-- --:--:-- --:--:--   603
100  114M  100  114M    0     0  1568k      0  0:01:15  0:01:15 --:--:-- 1562k

Built: nexus3-3.12.1.01-1.el7.centos.x86_64.rpm
```

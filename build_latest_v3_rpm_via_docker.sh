#!/bin/bash
echo "Cloning / updating nexus-oss-rpms.git"
# A fork of this repo is availabe here: https://github.com/drpauldixon/nexus-oss-rpms.git

[ ! -d nexus-oss-rpms ] && git clone https://github.com/juliogonzalez/nexus-oss-rpms.git
cd nexus-oss-rpms
git pull
cd ..
docker run -it -v $PWD:/scratch centos:7 /scratch/entrypoint.sh

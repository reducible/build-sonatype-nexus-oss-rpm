#!/bin/bash
echo "Installing package dependencies... this may take a few minutes"
yum -q -y install deltarpm
yum -q -y install rpm-build curl
cd /scratch/nexus-oss-rpms
touch timestamp
./nexus-oss-rpm -v 3 | grep -v "/scratch"
find RPMS -newer timestamp -name "*.rpm" -exec mv {} /scratch/ \;
[ -f timestamp ] && rm -f timestamp
cd /scratch
echo

set +e
RPM=$(ls -1rt *.rpm|tail -1)
if [ $? -eq 0 ]
then
  echo "Built: ${RPM}"
else
  echo "No RPM built"
  exit 1
fi
